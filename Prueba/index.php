<?php 
	include("settings/settings.php");
	include("backend/querys.php");
	$idcone=conexion();
	$ObtenerCliente=ObtenerCliente();
	$sql_a=mysqli_query($idcone, $ObtenerCliente);

	$ObtenerBiclicleta=ObtenerBiclicleta();
	$sql_b=mysqli_query($idcone, $ObtenerBiclicleta);
	
	$nombres="";
	$apellidos="";
	$cedula="";
	$direccion="";
	$telefono="";
	$celular="";
	$fecha_nacimiento="";
	$guardar = 0;
	
	if (!empty($_REQUEST['cedula'])){
		$ObtenerClienteCedula=ObtenerClienteCedula($_REQUEST['cedula']);
		$sql_c=mysqli_query($idcone, $ObtenerClienteCedula);
		$fila=mysqli_fetch_array($sql_c);
		// echo "<pre>".print_r($fila,true)."</pre>";

		$nombres .= "disabled='true' value ='".$fila['nombres']."'";
		$apellidos .= "disabled='true' value ='".$fila['apellidos']."'";
		$cedula .= "readonly='true' value ='".$fila['cedula']."'";
		
		$direccion .= "value ='".$fila['direccion']."'";
		$telefono .= "value ='".$fila['telefono']."'";
		$celular .= "value ='".$fila['celular']."'";
		$fecha_nacimiento .= "value ='".$fila['fecha_nacimiento']."'";
		$guardar = 1;
	}
	
?>
	
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport"  http-equiv="Content-Type" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;text/html; charset=UTF-8" />
	<title>Prueba</title>
	<link href="static_files/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
	  <h1>EcoBici</h1>
	  <p>La solución esta en nuestras manos..</p> 
	</div>

	<div class="container" style="margin-top:30px">
  		<div class="row">
	    	<div class="col-sm-6">
				<form  action="backend/ecobici.php" method="POST">
					<input type="number" name="guardar" value='<?php echo $guardar ?>' hidden="true">
					<div class="form-group">
						<label for="nombres">Nombres</label>
						<input type="text" class="form-control" id="nombres" name="nombres" placeholder="nombres" <?php echo $nombres ?> >
					</div>
					<div class="form-group">
						<label for="apellidos">Apellidos</label>
						<input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" <?php echo $apellidos ?>>
					</div>
					<div class="form-group">
						<label for="cedula">Cédula</label>
						<input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" <?php echo $cedula ?>>
					</div>

					<div class="form-group">
						<label for="direccion">Dirección</label>
						<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" <?php echo $direccion ?>>
					</div>

					<div class="form-group">
						<label for="telefono">Teléfono</label>
						<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" <?php echo $telefono ?>>
					</div>

					<div class="form-group">
						<label for="celular">Celular</label>
						<input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" <?php echo $celular ?>>
					</div>

					<div class="form-group">
						<label for="fecha_nacimiento">Fecha de nacimiento</label>
						<input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" <?php echo $fecha_nacimiento ?>>
					</div>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</form>
			</div>
			<div class="col-sm-5">
				<form  action="backend/bicicleta.php" method="POST">
					<div class="form-group">
						<label for="ubicacion">Id Ubicación</label>
						<input type="number" class="form-control" id="ubicacion" name="ubicacion" placeholder="ubicacion">
					</div>
					<div class="form-group">
						<label for="color">Color</label>
						<input type="color" class="form-control" id="color" name="color" placeholder="color">
					</div>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<table class="table">
				    <thead>
				      <tr>
				        <th>Nombres</th>
				        <th>Apellidos</th>
				        <th>Cédula</th>
				        <th>Dirección</th>
				        <th>Teléfono</th>
				        <th>Celular</th>
				        <th>Fecha de nacimiento</th>
				      </tr>
				    </thead>
				    <tbody>
				    <?php
						while($fila=mysqli_fetch_array($sql_a)){
					?>
				      <tr>
				        <td><?php echo $fila ['nombres'] ?></td>
				        <td><?php echo $fila ['apellidos'] ?></td>
				        <td><?php echo $fila ['cedula'] ?></td>
				        <td><?php echo $fila ['direccion'] ?></td>
				        <td><?php echo $fila ['telefono'] ?></td>
				        <td><?php echo $fila ['celular'] ?></td>
				        <td><?php echo $fila ['fecha_nacimiento'] ?></td>
				        <td>
				        	<a 
				        	href='index.php?cedula=<?php echo $fila ['cedula'] ?>'>
				        	<img title="Modificar Cliente" src="static_files/img/edit.png" width="16px" height="16px"></a>
				        </td>
				      </tr>
				    <?php 
						} 
					?>
				    </tbody>
				 </table>
			</div>
			<div class="col-sm-1">
				
			</div>
			<div class="col-sm-5">
				<table class="table">
				    <thead>
				      <tr>
				        <th>Id Ubicación</th>
				        <th>Color</th>
				        <th>Estado</th>
				      </tr>
				    </thead>
				    <tbody>
				    <?php
						while($fila=mysqli_fetch_array($sql_b)){
					?>
				      <tr>
				        <td><?php echo $fila ['ubicacion'] ?></td>
				        <td><?php echo $fila ['color'] ?></td>
				        <td><?php echo $fila ['estado'] ?></td>
				      </tr>
				    <?php 
						} 
					?>
				    </tbody>
				 </table>
			</div>
		</div>
	</div>
</body>
<script src="static_files/jquery/jquery-1.11.1.min.js"></script>
<script src="static_files/bootstrap/js/bootstrap.min.js"></script>
</html>