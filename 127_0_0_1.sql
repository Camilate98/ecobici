-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2018 a las 20:49:52
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd`
--
CREATE DATABASE IF NOT EXISTS `bd` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cli_clientes`
--

CREATE TABLE `cli_clientes` (
  `idcliente` int(11) NOT NULL,
  `cli_pnombre` varchar(50) NOT NULL,
  `cli_snombre` varchar(50) NOT NULL,
  `cli_papellido` varchar(50) NOT NULL,
  `cli_sapellido` varchar(50) NOT NULL,
  `cli_fnacimiento` date NOT NULL,
  `cli_tdocumento` int(11) NOT NULL,
  `cli_ndocumento` int(11) NOT NULL,
  `cli_ncelular` int(11) NOT NULL,
  `cli_email` varchar(50) NOT NULL,
  `cli_est_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emp_empleado`
--

CREATE TABLE `emp_empleado` (
  `idempleado` int(11) NOT NULL,
  `emp_nombres` varchar(30) NOT NULL,
  `emp_apellido` varchar(30) NOT NULL,
  `emp_id_docu` int(11) NOT NULL,
  `emp_ndocu` int(30) NOT NULL,
  `emp_email` varchar(30) NOT NULL,
  `emp_direccion` varchar(30) NOT NULL,
  `emp_numero` int(30) NOT NULL,
  `idest_emp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est_estado`
--

CREATE TABLE `est_estado` (
  `idestado` int(11) NOT NULL,
  `est_nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `est_estado`
--

INSERT INTO `est_estado` (`idestado`, `est_nombre`) VALUES
(1, 'Activo'),
(2, 'Inactivo'),
(3, 'Activo'),
(4, 'Inactivo'),
(5, 'Rol-Activo'),
(6, 'Rol-Inactivo'),
(7, 'Proveedor-Activo'),
(8, 'Proveedor-Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idproveedor` int(11) NOT NULL,
  `NIT_prov` int(50) NOT NULL,
  `nombre_prov` varchar(50) NOT NULL,
  `telefono_prov` int(50) NOT NULL,
  `direcion_prov` varchar(50) NOT NULL,
  `correo_prov` varchar(50) NOT NULL,
  `id_est_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`idproveedor`, `NIT_prov`, `nombre_prov`, `telefono_prov`, `direcion_prov`, `correo_prov`, `id_est_prov`) VALUES
(1, 444, 'cvcv', 21, 'nbnbn', '2121', 8),
(2, 0, 'asas', 42542, '', '', 7),
(3, 577, 'sds', 4545, 'sdd', '4', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_roles`
--

CREATE TABLE `rol_roles` (
  `idrol` int(11) NOT NULL,
  `rol_nombre` varchar(30) NOT NULL,
  `est_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol_roles`
--

INSERT INTO `rol_roles` (`idrol`, `rol_nombre`, `est_rol`) VALUES
(1, 'Ingenierod', 5),
(2, 'Gerente', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_docu`
--

CREATE TABLE `tipo_docu` (
  `iddocumento` int(11) NOT NULL,
  `docu_nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_docu`
--

INSERT INTO `tipo_docu` (`iddocumento`, `docu_nombre`) VALUES
(1, 'Cedula de Ciudadania'),
(2, 'Cedula Extranjera'),
(3, 'Tarjeta de identidad'),
(4, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_usuarios`
--

CREATE TABLE `usu_usuarios` (
  `idusuario` int(11) NOT NULL,
  `usu_usuario` varchar(30) NOT NULL,
  `usu_clave` varchar(30) NOT NULL,
  `usu_est_id` int(11) NOT NULL,
  `usu_rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usu_usuarios`
--

INSERT INTO `usu_usuarios` (`idusuario`, `usu_usuario`, `usu_clave`, `usu_est_id`, `usu_rol_id`) VALUES
(1, 'david', '12345', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cli_clientes`
--
ALTER TABLE `cli_clientes`
  ADD PRIMARY KEY (`idcliente`),
  ADD KEY `cliente1` (`cli_est_id`),
  ADD KEY `cliente3` (`cli_tdocumento`);

--
-- Indices de la tabla `emp_empleado`
--
ALTER TABLE `emp_empleado`
  ADD PRIMARY KEY (`idempleado`),
  ADD KEY `emp2` (`emp_id_docu`),
  ADD KEY `idest_emp` (`idest_emp`);

--
-- Indices de la tabla `est_estado`
--
ALTER TABLE `est_estado`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedor`),
  ADD KEY `id_est_prov` (`id_est_prov`);

--
-- Indices de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  ADD PRIMARY KEY (`idrol`),
  ADD KEY `est_rol` (`est_rol`);

--
-- Indices de la tabla `tipo_docu`
--
ALTER TABLE `tipo_docu`
  ADD PRIMARY KEY (`iddocumento`);

--
-- Indices de la tabla `usu_usuarios`
--
ALTER TABLE `usu_usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `usuario1` (`usu_est_id`),
  ADD KEY `usuario2` (`usu_rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cli_clientes`
--
ALTER TABLE `cli_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `emp_empleado`
--
ALTER TABLE `emp_empleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `emp_empleado`
--
ALTER TABLE `emp_empleado`
  ADD CONSTRAINT `emp2` FOREIGN KEY (`emp_id_docu`) REFERENCES `tipo_docu` (`iddocumento`),
  ADD CONSTRAINT `emp_empleado_ibfk_1` FOREIGN KEY (`idest_emp`) REFERENCES `rol_roles` (`idrol`);

--
-- Filtros para la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD CONSTRAINT `proveedores_ibfk_1` FOREIGN KEY (`id_est_prov`) REFERENCES `est_estado` (`idestado`);

--
-- Filtros para la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  ADD CONSTRAINT `rol_roles_ibfk_1` FOREIGN KEY (`est_rol`) REFERENCES `est_estado` (`idestado`);

--
-- Filtros para la tabla `usu_usuarios`
--
ALTER TABLE `usu_usuarios`
  ADD CONSTRAINT `usu_usuarios_ibfk_1` FOREIGN KEY (`usu_est_id`) REFERENCES `est_estado` (`idestado`),
  ADD CONSTRAINT `usu_usuarios_ibfk_2` FOREIGN KEY (`usu_rol_id`) REFERENCES `rol_roles` (`idrol`);
--
-- Base de datos: `dbdrug`
--
CREATE DATABASE IF NOT EXISTS `dbdrug` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbdrug`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cars_tmp`
--

CREATE TABLE `cars_tmp` (
  `id_cars_tmp` int(11) NOT NULL,
  `id_productos` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cars_cantidad` int(11) NOT NULL,
  `cars_alive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cars_tmp`
--

INSERT INTO `cars_tmp` (`id_cars_tmp`, `id_productos`, `id_usuario`, `cars_cantidad`, `cars_alive`) VALUES
(30, 1, 10, 1, 1),
(31, 1, 12, 1, 1),
(32, 3, 12, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `c_nombre` varchar(50) NOT NULL,
  `c_apellido` varchar(50) NOT NULL,
  `c_telefono` varchar(13) NOT NULL,
  `c_direccion` varchar(20) NOT NULL,
  `c_ciudad` varchar(20) NOT NULL,
  `c_correo` varchar(100) NOT NULL,
  `c_contraseña` varchar(30) NOT NULL,
  `cli_usu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `c_nombre`, `c_apellido`, `c_telefono`, `c_direccion`, `c_ciudad`, `c_correo`, `c_contraseña`, `cli_usu_id`) VALUES
(1, 'Leidy Katerine', 'Charrupi', '8810346', 'Cra 21 No. 19B-64', 'Cali', 'lkc@gmail.com', '12345678', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion`
--

CREATE TABLE `documentacion` (
  `id_docu` int(11) NOT NULL,
  `docu_prefijo` varchar(11) NOT NULL,
  `docu_consecutivo` int(11) NOT NULL,
  `docu_ini` int(11) NOT NULL,
  `docu_fin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `documentacion`
--

INSERT INTO `documentacion` (`id_docu`, `docu_prefijo`, `docu_consecutivo`, `docu_ini`, `docu_fin`) VALUES
(2, 'F', 0, 1, 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_factura`
--

CREATE TABLE `estados_factura` (
  `id_estado` int(11) NOT NULL,
  `id_descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `estados_factura`
--

INSERT INTO `estados_factura` (`id_estado`, `id_descripcion`) VALUES
(1, 'Pedido'),
(2, 'Factura'),
(3, 'Despachado'),
(4, 'Elaboración'),
(5, 'Pendiente'),
(6, 'Despachado'),
(7, 'Consignacion'),
(8, 'Guia'),
(9, 'Confirmado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fact_detalle`
--

CREATE TABLE `fact_detalle` (
  `deta_id` int(11) NOT NULL,
  `fact_id` int(11) NOT NULL,
  `deta_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deta_produ_id` int(11) NOT NULL,
  `deta_impuesto` int(11) NOT NULL,
  `deta_descuento` int(11) NOT NULL,
  `deta_valorunitario` int(11) NOT NULL,
  `deta_cant` int(11) NOT NULL,
  `deta_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fact_detalle`
--

INSERT INTO `fact_detalle` (`deta_id`, `fact_id`, `deta_date`, `deta_produ_id`, `deta_impuesto`, `deta_descuento`, `deta_valorunitario`, `deta_cant`, `deta_total`) VALUES
(350, 166, '2018-07-12 19:12:52', 1, 0, 0, 1000, 1, 1000),
(351, 166, '2018-07-12 19:12:53', 3, 0, 0, 8000, 1, 8000),
(352, 166, '2018-07-12 19:12:53', 4, 0, 0, 3500, 1, 3500),
(353, 166, '2018-07-12 19:15:12', 1, 0, 0, 1000, 1, 1000),
(354, 166, '2018-07-12 19:15:12', 3, 0, 0, 8000, 1, 8000),
(355, 166, '2018-07-12 19:15:13', 4, 0, 0, 3500, 1, 3500),
(356, 168, '2018-07-12 19:16:26', 1, 0, 0, 1000, 1, 1000),
(357, 168, '2018-07-12 19:16:26', 3, 0, 0, 8000, 1, 8000),
(358, 168, '2018-07-12 19:16:26', 4, 0, 0, 3500, 1, 3500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `fact_id` int(11) NOT NULL,
  `fact_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fact_est_id` int(11) NOT NULL,
  `fact_cli_id` int(11) NOT NULL,
  `fact_prefijo_id` int(11) NOT NULL,
  `fact_consecutivo` int(11) NOT NULL,
  `fact_impuesto` double NOT NULL,
  `fact_descuento` double NOT NULL,
  `fact_total` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`fact_id`, `fact_date`, `fact_est_id`, `fact_cli_id`, `fact_prefijo_id`, `fact_consecutivo`, `fact_impuesto`, `fact_descuento`, `fact_total`) VALUES
(166, '2018-07-12 19:12:52', 1, 1, 0, 0, 0, 0, 0),
(168, '2018-07-12 19:16:26', 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_productos` int(11) NOT NULL,
  `p_descripcion` varchar(50) NOT NULL,
  `p_cantidad` int(11) NOT NULL,
  `p_valor_costo` int(100) NOT NULL,
  `p_descuento` int(11) NOT NULL,
  `p_margen` double NOT NULL,
  `p_valor_comercial` int(100) NOT NULL,
  `p_alive` tinyint(1) NOT NULL DEFAULT '1',
  `p_root` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_productos`, `p_descripcion`, `p_cantidad`, `p_valor_costo`, `p_descuento`, `p_margen`, `p_valor_comercial`, `p_alive`, `p_root`) VALUES
(1, 'Acetaminofen 500mg', 5, 5000, 20, 10, 1000, 1, 'static/modules/main/img/acetaminofen.jpg'),
(3, 'Amoxicilina 500mg', 2, 4000, 10, 20, 8000, 1, 'static/modules/main/img/amoxicilina.jpg'),
(4, 'Aspirina 500mg', 2, 2500, 10, 30, 3500, 1, 'static/modules/main/img/aspirina.jpg'),
(5, 'Bisolvon Infantil', 5, 4500, 10, 15, 8600, 1, 'static/modules/main/img/bisolvon.jpg'),
(6, 'Calmidol', 5, 3600, 20, 10, 4500, 1, 'static/modules/main/img/calmidol.jpg\r\n'),
(7, 'ChapStick', 12, 5400, 20, 12, 9400, 1, 'static/modules/main/img/chapStick.jpg'),
(8, 'Clotrimazol', 2, 14000, 20, 10, 25000, 1, 'static/modules/main/img/clotrimazol.jpg'),
(9, 'Dolex', 15, 4200, 15, 25, 5600, 1, 'static/modules/main/img/dolex.jpg\r\n'),
(10, 'Gine-Canesten 20mg', 4, 14200, 10, 24, 18200, 1, 'static/modules/main/img/ginocanesten.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rol_descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `rol_descripcion`) VALUES
(1, 'Admin'),
(2, 'Cliente'),
(3, 'Sin rol');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `id_tareas` int(11) NOT NULL,
  `hw_asunto` text NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(1) NOT NULL DEFAULT '1' COMMENT '1->pendiente, 2->cerrado',
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`id_tareas`, `hw_asunto`, `create_at`, `estado`, `id_usuario`) VALUES
(173, 'se ha generado el pedido PEDIDO-166', '2018-07-12 19:15:13', 1, 1),
(174, 'se ha generado el pedido PEDIDO-168', '2018-07-12 19:16:26', 1, 1),
(175, 'Se ha registrado el cliente con identificaciÃ³n 12345675', '2018-07-12 19:27:07', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `id_trans` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `trans_fact_id` int(11) NOT NULL,
  `trans_prefijo` varchar(11) NOT NULL,
  `trans_fact_est_id` int(11) NOT NULL,
  `trans_cli_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `u_nombre` varchar(50) NOT NULL,
  `u_login` varchar(100) NOT NULL,
  `u_contrasena` varchar(50) NOT NULL,
  `u_identificacion` int(11) NOT NULL,
  `u_estado_id` int(11) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `u_roles_id` int(11) NOT NULL,
  `u_alive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `u_nombre`, `u_login`, `u_contrasena`, `u_identificacion`, `u_estado_id`, `u_email`, `u_roles_id`, `u_alive`) VALUES
(1, 'dn', 'dn', 'dn', 1234, 7, 'dianaar190@gmail.com', 1, 1),
(10, 'sos', 'so', 'so', 11, 7, 'so@hotmail.com', 2, 1),
(11, 'op', 'op', 'op', 12340, 7, 'sospina@hotmail.com', 3, 1),
(12, 'Sebastian', 'sospina', 'sospina', 12345675, 7, 'sebastian.ospina95@hotmail.com', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cars_tmp`
--
ALTER TABLE `cars_tmp`
  ADD PRIMARY KEY (`id_cars_tmp`),
  ADD KEY `id_productos` (`id_productos`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD PRIMARY KEY (`id_docu`);

--
-- Indices de la tabla `estados_factura`
--
ALTER TABLE `estados_factura`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `fact_detalle`
--
ALTER TABLE `fact_detalle`
  ADD PRIMARY KEY (`deta_id`),
  ADD KEY `deta_produ_id` (`deta_produ_id`),
  ADD KEY `fact_id` (`fact_id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`fact_id`),
  ADD KEY `fact_est_id` (`fact_est_id`),
  ADD KEY `fact_cli_id` (`fact_cli_id`),
  ADD KEY `fact_prefijo_id` (`fact_prefijo_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_productos`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id_tareas`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`id_trans`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `u_roles_id` (`u_roles_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cars_tmp`
--
ALTER TABLE `cars_tmp`
  MODIFY `id_cars_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  MODIFY `id_docu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `estados_factura`
--
ALTER TABLE `estados_factura`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `fact_detalle`
--
ALTER TABLE `fact_detalle`
  MODIFY `deta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=359;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `fact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_productos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `id_tareas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `id_trans` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cars_tmp`
--
ALTER TABLE `cars_tmp`
  ADD CONSTRAINT `cars_tmp_ibfk_1` FOREIGN KEY (`id_productos`) REFERENCES `productos` (`id_productos`),
  ADD CONSTRAINT `cars_tmp_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `fact_detalle`
--
ALTER TABLE `fact_detalle`
  ADD CONSTRAINT `fact_detalle_ibfk_2` FOREIGN KEY (`deta_produ_id`) REFERENCES `productos` (`id_productos`),
  ADD CONSTRAINT `fact_detalle_ibfk_3` FOREIGN KEY (`fact_id`) REFERENCES `factura` (`fact_id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`fact_est_id`) REFERENCES `estados_factura` (`id_estado`),
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`fact_cli_id`) REFERENCES `cliente` (`id_cliente`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`u_roles_id`) REFERENCES `roles` (`id`);
--
-- Base de datos: `ecobici`
--
CREATE DATABASE IF NOT EXISTS `ecobici` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecobici`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bicicleta`
--

CREATE TABLE `bicicleta` (
  `ubicacion` int(11) NOT NULL,
  `color` varchar(30) NOT NULL,
  `estado` varchar(30) NOT NULL DEFAULT 'Libre'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bicicleta`
--

INSERT INTO `bicicleta` (`ubicacion`, `color`, `estado`) VALUES
(123123, '#004080', 'Libre'),
(12314412, '#000000', 'Libre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `cedula` varchar(30) NOT NULL,
  `direccion` text NOT NULL,
  `telefono` varchar(13) NOT NULL,
  `celular` varchar(13) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `estado` varchar(30) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`nombres`, `apellidos`, `cedula`, `direccion`, `telefono`, `celular`, `fecha_nacimiento`, `estado`) VALUES
('Sebastian', 'Ospina Hurtado', '1143858325', 'calle 45 # 1-1', '7896352', '315478956', '2018-06-04', 'Activo'),
('Diana Arboleda', 'Zapata', '1144206314', 'Calle 78 # 45-96', '4478596', '315456523', '2018-11-30', 'Activo'),
('Juan', 'Camilo Alzate', '143965895', 'Calle 78 # 75-96', '4356789', '148956325', '2018-11-30', 'Activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bicicleta`
--
ALTER TABLE `bicicleta`
  ADD PRIMARY KEY (`ubicacion`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cedula`);
--
-- Base de datos: `ing_software`
--
CREATE DATABASE IF NOT EXISTS `ing_software` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ing_software`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accidente_estudiante`
--

CREATE TABLE `accidente_estudiante` (
  `id_accidente_estu` int(11) NOT NULL,
  `numero_estu_doc` int(11) NOT NULL,
  `fecha_acci` date NOT NULL,
  `descrip_accide` varchar(200) NOT NULL,
  `lugar_acci` varchar(45) NOT NULL,
  `emermedica_estu_id` int(11) NOT NULL,
  `reporte_emermedica` varchar(200) NOT NULL,
  `incapacidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emermedica`
--

CREATE TABLE `emermedica` (
  `id_emermedica` int(12) NOT NULL,
  `descripcion_emer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes`
--

CREATE TABLE `estudiantes` (
  `id_estudiante` int(12) NOT NULL,
  `tipo_doc_estu` int(11) NOT NULL,
  `numero_doc_estu` int(11) NOT NULL,
  `nombre_estu` varchar(100) NOT NULL,
  `apellidos_estu` varchar(100) NOT NULL,
  `contacto_estu` varchar(100) NOT NULL,
  `email_estu` varchar(100) NOT NULL,
  `id_jornada` int(11) NOT NULL,
  `eps_estu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incapacidades`
--

CREATE TABLE `incapacidades` (
  `id_incapacidad` int(11) NOT NULL,
  `codigo_incapacidad` int(11) NOT NULL,
  `causa_incapacidad` varchar(100) NOT NULL,
  `incio_incapacidad` date NOT NULL,
  `fin_incapacidad` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `id_jornada` int(11) NOT NULL,
  `descrip_jornada` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `tip_doc_id` int(12) NOT NULL,
  `tipo_doc_descrip` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accidente_estudiante`
--
ALTER TABLE `accidente_estudiante`
  ADD PRIMARY KEY (`id_accidente_estu`);

--
-- Indices de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  ADD PRIMARY KEY (`id_estudiante`),
  ADD KEY `id_jornada` (`id_jornada`),
  ADD KEY `tipo_doc_estu` (`tipo_doc_estu`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`id_jornada`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`tip_doc_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accidente_estudiante`
--
ALTER TABLE `accidente_estudiante`
  MODIFY `id_accidente_estu` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  MODIFY `id_estudiante` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
  MODIFY `id_jornada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `tip_doc_id` int(12) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `accidente_estudiante`
--
ALTER TABLE `accidente_estudiante`
  ADD CONSTRAINT `accidente_estudiante_ibfk_1` FOREIGN KEY (`id_accidente_estu`) REFERENCES `estudiantes` (`id_estudiante`);

--
-- Filtros para la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  ADD CONSTRAINT `estudiantes_ibfk_1` FOREIGN KEY (`id_jornada`) REFERENCES `jornada` (`id_jornada`),
  ADD CONSTRAINT `estudiantes_ibfk_2` FOREIGN KEY (`tipo_doc_estu`) REFERENCES `tipo_documento` (`tip_doc_id`);
--
-- Base de datos: `mydb`
--
CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mydb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente`
--

CREATE TABLE `agente` (
  `id_Agente` int(11) NOT NULL,
  `Age_nombre` varchar(45) NOT NULL,
  `age_apellido` varchar(45) NOT NULL,
  `age_placa` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comparendo`
--

CREATE TABLE `comparendo` (
  `idcomparendo` int(11) NOT NULL,
  `compa_fecha` datetime(6) NOT NULL,
  `compa_municipio` varchar(45) NOT NULL,
  `compa_direccion` varchar(45) NOT NULL,
  `Infraccion_idInfraccion` int(11) NOT NULL,
  `Agente_id_Agente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `idCurso` int(11) NOT NULL,
  `curso_fecha` date NOT NULL,
  `Valor_Descuento` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion`
--

CREATE TABLE `facturacion` (
  `idFacturacion` int(11) NOT NULL,
  `valor_descuento` double DEFAULT NULL,
  `total_pagar` double DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `Curso_idCurso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infraccion`
--

CREATE TABLE `infraccion` (
  `idInfraccion` int(11) NOT NULL,
  `tipo_infraccion` varchar(45) NOT NULL,
  `Salario_idSalario` int(11) NOT NULL,
  `Curso_idCurso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salario`
--

CREATE TABLE `salario` (
  `idSalario` int(11) NOT NULL,
  `año_salario` date NOT NULL,
  `valor_salario` double NOT NULL,
  `no_salarios` int(11) NOT NULL,
  `Facturacion_idFacturacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_vehiculo`
--

CREATE TABLE `servicio_vehiculo` (
  `tipo_servicio` varchar(45) NOT NULL,
  `modalidad_trans` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_vehiculo`
--

CREATE TABLE `tipo_vehiculo` (
  `tipo_vehiculo` varchar(45) NOT NULL,
  `vehiculo_no_placa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='																																																																																																																																																																																																																																																																																																											';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usu_no_documento` int(11) NOT NULL,
  `usu_nombre` varchar(45) NOT NULL,
  `usu_apellido` varchar(45) NOT NULL,
  `usu_telefono` varchar(45) NOT NULL,
  `usu_direccion` varchar(45) NOT NULL,
  `usu_no_licencia` int(11) NOT NULL,
  `usu_ciudad` varchar(45) DEFAULT NULL,
  `usu_tipo_usu` varchar(45) DEFAULT NULL,
  `tipo_vehiculo_tipo_vehiculo` varchar(45) NOT NULL,
  `comparendo_idcomparendo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `no_placa` int(11) NOT NULL,
  `año_vehiculo` date DEFAULT NULL,
  `servicio_vehiculo_tipo_servicio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agente`
--
ALTER TABLE `agente`
  ADD PRIMARY KEY (`id_Agente`);

--
-- Indices de la tabla `comparendo`
--
ALTER TABLE `comparendo`
  ADD PRIMARY KEY (`idcomparendo`),
  ADD KEY `fk_comparendo_Infraccion1_idx` (`Infraccion_idInfraccion`),
  ADD KEY `fk_comparendo_Agente1_idx` (`Agente_id_Agente`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indices de la tabla `facturacion`
--
ALTER TABLE `facturacion`
  ADD PRIMARY KEY (`idFacturacion`),
  ADD KEY `fk_Facturacion_Curso1_idx` (`Curso_idCurso`);

--
-- Indices de la tabla `infraccion`
--
ALTER TABLE `infraccion`
  ADD PRIMARY KEY (`idInfraccion`,`Curso_idCurso`),
  ADD KEY `fk_Infraccion_Salario1_idx` (`Salario_idSalario`),
  ADD KEY `fk_Infraccion_Curso1_idx` (`Curso_idCurso`);

--
-- Indices de la tabla `salario`
--
ALTER TABLE `salario`
  ADD PRIMARY KEY (`idSalario`),
  ADD KEY `fk_Salario_Facturacion1_idx` (`Facturacion_idFacturacion`);

--
-- Indices de la tabla `servicio_vehiculo`
--
ALTER TABLE `servicio_vehiculo`
  ADD PRIMARY KEY (`tipo_servicio`);

--
-- Indices de la tabla `tipo_vehiculo`
--
ALTER TABLE `tipo_vehiculo`
  ADD PRIMARY KEY (`tipo_vehiculo`),
  ADD KEY `fk_tipo_vehiculo_vehiculo1_idx` (`vehiculo_no_placa`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usu_no_documento`),
  ADD KEY `fk_Usuario_tipo_vehiculo_idx` (`tipo_vehiculo_tipo_vehiculo`),
  ADD KEY `fk_Usuario_comparendo1_idx` (`comparendo_idcomparendo`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`no_placa`),
  ADD KEY `fk_vehiculo_servicio_vehiculo1_idx` (`servicio_vehiculo_tipo_servicio`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comparendo`
--
ALTER TABLE `comparendo`
  ADD CONSTRAINT `fk_comparendo_Agente1` FOREIGN KEY (`Agente_id_Agente`) REFERENCES `agente` (`id_Agente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comparendo_Infraccion1` FOREIGN KEY (`Infraccion_idInfraccion`) REFERENCES `infraccion` (`idInfraccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `facturacion`
--
ALTER TABLE `facturacion`
  ADD CONSTRAINT `fk_Facturacion_Curso1` FOREIGN KEY (`Curso_idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `infraccion`
--
ALTER TABLE `infraccion`
  ADD CONSTRAINT `fk_Infraccion_Curso1` FOREIGN KEY (`Curso_idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Infraccion_Salario1` FOREIGN KEY (`Salario_idSalario`) REFERENCES `salario` (`idSalario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `salario`
--
ALTER TABLE `salario`
  ADD CONSTRAINT `fk_Salario_Facturacion1` FOREIGN KEY (`Facturacion_idFacturacion`) REFERENCES `facturacion` (`idFacturacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tipo_vehiculo`
--
ALTER TABLE `tipo_vehiculo`
  ADD CONSTRAINT `fk_tipo_vehiculo_vehiculo1` FOREIGN KEY (`vehiculo_no_placa`) REFERENCES `vehiculo` (`no_placa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_Usuario_comparendo1` FOREIGN KEY (`comparendo_idcomparendo`) REFERENCES `comparendo` (`idcomparendo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Usuario_tipo_vehiculo` FOREIGN KEY (`tipo_vehiculo_tipo_vehiculo`) REFERENCES `tipo_vehiculo` (`tipo_vehiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD CONSTRAINT `fk_vehiculo_servicio_vehiculo1` FOREIGN KEY (`servicio_vehiculo_tipo_servicio`) REFERENCES `servicio_vehiculo` (`tipo_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;
--
-- Base de datos: `parking`
--
CREATE DATABASE IF NOT EXISTS `parking` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `parking`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `tipo_docu_id` int(5) NOT NULL,
  `tipo_docu_nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usu_id` int(11) NOT NULL,
  `usu_login` varchar(15) NOT NULL,
  `usu_nombres` varchar(50) NOT NULL,
  `usu_tipo_doc` int(5) NOT NULL,
  `usu_nodoc` int(15) NOT NULL,
  `usu_est_id` int(5) NOT NULL,
  `usu_rol_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`tipo_docu_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usu_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `tipo_docu_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT;--
-- Base de datos: `parqueadero`
--
CREATE DATABASE IF NOT EXISTS `parqueadero` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `parqueadero`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est_estados`
--

CREATE TABLE `est_estados` (
  `est_id` int(11) NOT NULL,
  `est_nombre` varchar(15) NOT NULL,
  `est_gre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gre_gruposestados`
--

CREATE TABLE `gre_gruposestados` (
  `gre_id` int(11) NOT NULL,
  `gre_nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_roles`
--

CREATE TABLE `rol_roles` (
  `rol_id` int(11) NOT NULL,
  `rol_descripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tdo_tiposdoc`
--

CREATE TABLE `tdo_tiposdoc` (
  `tdo_id` int(11) NOT NULL,
  `tdo_nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tve_tiposveh`
--

CREATE TABLE `tve_tiposveh` (
  `tve_id` int(11) NOT NULL,
  `tve_nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_usuarios`
--

CREATE TABLE `usu_usuarios` (
  `usu_id` int(11) NOT NULL,
  `usu_nolicencia` varchar(20) NOT NULL,
  `usu_login` varchar(20) NOT NULL,
  `usu_clave` varchar(50) NOT NULL,
  `usu_rol_id` int(11) NOT NULL,
  `usu_est_id` int(11) NOT NULL,
  `usu_nombre` varchar(50) NOT NULL,
  `usu_telefono` varchar(10) NOT NULL,
  `usu_direccion` varchar(50) NOT NULL,
  `usu_veh_id` int(11) NOT NULL,
  `usu_tdo_id` int(11) NOT NULL,
  `usu_doc` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `veh_vehiculos`
--

CREATE TABLE `veh_vehiculos` (
  `veh_id` int(11) NOT NULL,
  `veh_placa` varchar(10) NOT NULL,
  `veh_tve_id` int(11) NOT NULL,
  `veh_est_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `est_estados`
--
ALTER TABLE `est_estados`
  ADD PRIMARY KEY (`est_id`),
  ADD KEY `est_gre_id` (`est_gre_id`);

--
-- Indices de la tabla `gre_gruposestados`
--
ALTER TABLE `gre_gruposestados`
  ADD PRIMARY KEY (`gre_id`);

--
-- Indices de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indices de la tabla `tdo_tiposdoc`
--
ALTER TABLE `tdo_tiposdoc`
  ADD PRIMARY KEY (`tdo_id`);

--
-- Indices de la tabla `tve_tiposveh`
--
ALTER TABLE `tve_tiposveh`
  ADD PRIMARY KEY (`tve_id`);

--
-- Indices de la tabla `usu_usuarios`
--
ALTER TABLE `usu_usuarios`
  ADD PRIMARY KEY (`usu_id`),
  ADD KEY `usu_est_id` (`usu_est_id`),
  ADD KEY `usu_rol_id` (`usu_rol_id`),
  ADD KEY `usu_tdo_id` (`usu_tdo_id`),
  ADD KEY `usu_veh_id` (`usu_veh_id`);

--
-- Indices de la tabla `veh_vehiculos`
--
ALTER TABLE `veh_vehiculos`
  ADD PRIMARY KEY (`veh_id`),
  ADD KEY `veh_est_id` (`veh_est_id`),
  ADD KEY `veh_tve_id` (`veh_tve_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `est_estados`
--
ALTER TABLE `est_estados`
  MODIFY `est_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gre_gruposestados`
--
ALTER TABLE `gre_gruposestados`
  MODIFY `gre_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tdo_tiposdoc`
--
ALTER TABLE `tdo_tiposdoc`
  MODIFY `tdo_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tve_tiposveh`
--
ALTER TABLE `tve_tiposveh`
  MODIFY `tve_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usu_usuarios`
--
ALTER TABLE `usu_usuarios`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `veh_vehiculos`
--
ALTER TABLE `veh_vehiculos`
  MODIFY `veh_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `est_estados`
--
ALTER TABLE `est_estados`
  ADD CONSTRAINT `est_estados_ibfk_1` FOREIGN KEY (`est_gre_id`) REFERENCES `gre_gruposestados` (`gre_id`);

--
-- Filtros para la tabla `usu_usuarios`
--
ALTER TABLE `usu_usuarios`
  ADD CONSTRAINT `usu_usuarios_ibfk_1` FOREIGN KEY (`usu_est_id`) REFERENCES `est_estados` (`est_id`),
  ADD CONSTRAINT `usu_usuarios_ibfk_2` FOREIGN KEY (`usu_rol_id`) REFERENCES `rol_roles` (`rol_id`),
  ADD CONSTRAINT `usu_usuarios_ibfk_3` FOREIGN KEY (`usu_tdo_id`) REFERENCES `tdo_tiposdoc` (`tdo_id`),
  ADD CONSTRAINT `usu_usuarios_ibfk_4` FOREIGN KEY (`usu_veh_id`) REFERENCES `veh_vehiculos` (`veh_id`);

--
-- Filtros para la tabla `veh_vehiculos`
--
ALTER TABLE `veh_vehiculos`
  ADD CONSTRAINT `veh_vehiculos_ibfk_1` FOREIGN KEY (`veh_est_id`) REFERENCES `est_estados` (`est_id`),
  ADD CONSTRAINT `veh_vehiculos_ibfk_2` FOREIGN KEY (`veh_tve_id`) REFERENCES `tve_tiposveh` (`tve_id`);
--
-- Base de datos: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

--
-- Volcado de datos para la tabla `pma__designer_settings`
--

INSERT INTO `pma__designer_settings` (`username`, `settings_data`) VALUES
('root', '{\"angular_direct\":\"direct\",\"snap_to_grid\":\"off\",\"relation_lines\":\"true\",\"full_screen\":\"on\",\"small_big_all\":\">\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

--
-- Volcado de datos para la tabla `pma__navigationhiding`
--

INSERT INTO `pma__navigationhiding` (`username`, `item_name`, `item_type`, `db_name`, `table_name`) VALUES
('root', 'tareas', 'table', 'dbdrug', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Volcado de datos para la tabla `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"ecobici\",\"table\":\"cliente\"},{\"db\":\"ecobici\",\"table\":\"bicicleta\"},{\"db\":\"dbdrug\",\"table\":\"factura\"},{\"db\":\"dbdrug\",\"table\":\"cars_tmp\"},{\"db\":\"dbdrug\",\"table\":\"tareas\"},{\"db\":\"dbdrug\",\"table\":\"estados_factura\"},{\"db\":\"dbdrug\",\"table\":\"fact_detalle\"},{\"db\":\"dbdrug\",\"table\":\"productos\"},{\"db\":\"dbdrug\",\"table\":\"roles\"},{\"db\":\"dbdrug\",\"table\":\"transacciones\"}]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Volcado de datos para la tabla `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'dbdrug', 'cars_tmp', '{\"sorted_col\":\"`id_productos` ASC\"}', '2018-07-12 23:33:53'),
('root', 'proyecto1', 'cli_clientes', '{\"sorted_col\":\"`cli_clientes`.`cli_id` ASC\"}', '2017-05-07 22:14:57'),
('root', 'proyecto1', 'est_estado', '{\"sorted_col\":\"`est_estado`.`est_id` ASC\"}', '2017-10-12 04:16:40'),
('root', 'proyecto1', 'menu_menu', '{\"sorted_col\":\"`menu_menu`.`id_menu`  ASC\"}', '2017-09-05 19:18:16'),
('root', 'proyecto1', 'produ_producto', '{\"sorted_col\":\"`produ_producto`.`produ_id` ASC\"}', '2017-05-22 19:00:47'),
('root', 'proyecto1', 'rol_roles', '{\"sorted_col\":\"`rol_id` ASC\",\"CREATE_TIME\":\"2017-04-25 23:33:23\",\"col_order\":[\"0\",\"1\",\"2\"],\"col_visib\":[\"1\",\"1\",\"1\"]}', '2017-05-16 20:42:40'),
('root', 'proyecto1', 'sub_menu', '{\"sorted_col\":\"`sub_menu`.`sub_menu_id` ASC\"}', '2017-09-04 20:36:34'),
('root', 'proyecto1', 'submenu_usu', '{\"sorted_col\":\"`usu_id` ASC\"}', '2017-11-24 00:49:16'),
('root', 'proyecto1', 'tipo_doc_tipodocumento', '{\"sorted_col\":\"`tipo_doc_tipodocumento`.`tipo_doc_id` ASC\"}', '2017-03-07 01:36:46'),
('root', 'proyecto1', 'usu_usuario', '{\"sorted_col\":\"`usu_login`  ASC\"}', '2017-10-17 03:25:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Volcado de datos para la tabla `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2017-10-11 17:31:30', '{\"lang\":\"es\",\"collation_connection\":\"utf8mb4_unicode_ci\",\"ThemeDefault\":\"pmahomme\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indices de la tabla `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indices de la tabla `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indices de la tabla `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indices de la tabla `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indices de la tabla `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indices de la tabla `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indices de la tabla `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indices de la tabla `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indices de la tabla `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indices de la tabla `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indices de la tabla `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indices de la tabla `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indices de la tabla `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de datos: `proyecto1`
--
CREATE DATABASE IF NOT EXISTS `proyecto1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `proyecto1`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cli_clientes`
--

CREATE TABLE `cli_clientes` (
  `cli_id` int(11) NOT NULL,
  `cli_tipo_documento` int(11) NOT NULL,
  `cli_no_documento` varchar(30) NOT NULL,
  `cli_nombre1` varchar(50) NOT NULL,
  `cli_nombre2` varchar(50) NOT NULL,
  `cli_apellido1` varchar(50) NOT NULL,
  `cli_apellido2` varchar(50) NOT NULL,
  `cli_contacto` varchar(20) NOT NULL,
  `cli_email` varchar(50) NOT NULL,
  `cli_estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cli_clientes`
--

INSERT INTO `cli_clientes` (`cli_id`, `cli_tipo_documento`, `cli_no_documento`, `cli_nombre1`, `cli_nombre2`, `cli_apellido1`, `cli_apellido2`, `cli_contacto`, `cli_email`, `cli_estado`) VALUES
(1, 1, '111', 'Lorena', '', 'Zapata', 'Meda', '444', 'HET@HMAIL.COM', 2),
(2, 1, '1234', 'Oscar', 'Mera', 'jj', 'jjj', 'jj', 'jjsaa@gmail.com', 1),
(3, 2, '1234', 'ksajdaksj', 'ksjdak', 'kjsd', 'kjksdj', 'jksajak', 'jjsaa@gmail.com', 1),
(4, 2, '4487', 'Dorotea ', '', 'Mesa', '', '561365', 'dianaarboleda_@gmail.com', 2),
(5, 1, '4487', 'sss', '', 'dd', '', '561365', 'dianaarboleda_@gmail.com', 1),
(6, 1, 'qwdq', 'sd', 'sdassad', 'dss', 'dsfds', 'dsds', 'dianaarboleda_@gmail.com', 1),
(7, 2, '98052251435', 'Hector', 'Ivan', 'Arboleda', 'Zapata', '4423473', 'hem@hotmail.com', 1),
(8, 3, '31969008', 'Adriana ', 'Milena', 'Zapata', 'Mesa', '4423473', 'adri45@gmial.com', 1),
(9, 3, '1234', 'Luis', '', 'Gonzales', '', '44423473', 'asd@gmail.com', 2),
(10, 1, '1234|', 'Nelson', '', 'Zambrano', '', '315377888', 'skjfskjds@gmail.com', 1),
(11, 5, '4487', 'Sebastian', '', 'Ospina', 'Hurtado', '3104306113', 'sebas.h@hotmail.com', 1),
(12, 1, '1144206146', 'Diana', 'Marcela', 'Arboleda', 'Zapata', '3137835385', 'dianaarboleda_@hotmail.com', 1),
(13, 1, 'a', 'd', 'd', 'd', 'd', '5', 'ddd@hss.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est_estado`
--

CREATE TABLE `est_estado` (
  `est_id` int(11) NOT NULL,
  `est_nombre` varchar(50) NOT NULL,
  `est_gru_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `est_estado`
--

INSERT INTO `est_estado` (`est_id`, `est_nombre`, `est_gru_id`) VALUES
(1, 'Activo', 2),
(2, 'Inactivo', 2),
(3, 'Activo', 1),
(4, 'Inactivo', 1),
(5, 'Activo', 3),
(6, 'Inactivo', 3),
(7, 'Activo', 4),
(8, 'Inactivo', 4),
(9, 'Activo', 5),
(10, 'Inactivo', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gru_grupos`
--

CREATE TABLE `gru_grupos` (
  `gru_id` int(11) NOT NULL,
  `gru_nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gru_grupos`
--

INSERT INTO `gru_grupos` (`gru_id`, `gru_nombre`) VALUES
(1, 'Usuario'),
(2, 'Cliente'),
(3, 'Roles'),
(4, 'Producto'),
(5, 'Proveedores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_menu`
--

CREATE TABLE `menu_menu` (
  `id_menu` int(11) NOT NULL,
  `menu_nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu_menu`
--

INSERT INTO `menu_menu` (`id_menu`, `menu_nombre`) VALUES
(1, 'Gestion de Clientes'),
(2, 'Gestion usuarios'),
(3, 'Gestion roles'),
(4, 'Gestion proveedor'),
(5, 'Gestion producto'),
(6, 'Gestion menu'),
(7, 'XXXX'),
(8, 'Gestion comercial'),
(9, 'Mercancia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produ_producto`
--

CREATE TABLE `produ_producto` (
  `produ_id` int(11) NOT NULL,
  `produ_referencia` varchar(50) NOT NULL,
  `produ_nombre` varchar(50) NOT NULL,
  `produ_cantidad` int(11) NOT NULL,
  `produ_prove_id` int(11) NOT NULL,
  `produ_est_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `produ_producto`
--

INSERT INTO `produ_producto` (`produ_id`, `produ_referencia`, `produ_nombre`, `produ_cantidad`, `produ_prove_id`, `produ_est_id`) VALUES
(14, 'Cartulina_ver_pliego', 'Cartulina verde', 200, 10, 7),
(15, 'Carton_octavo', 'Carton', 50, 3, 7),
(16, 'Carton_octavo', 'Carton Paja', 50, 9, 8),
(17, 'AQUI ESTOY', 'Hola', 5, 10, 7),
(18, 'XXXX', 'XXXX', 22, 1, 7),
(19, 'xxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxx', 55, 1, 7),
(20, 'xxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxx', 55, 1, 7),
(21, 'xxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxxx', 55, 1, 7),
(22, 'sasdaskdjksadja', 'Cartulina', 8888, 9, 8),
(23, 'PRUEBA_PRUEBA', 'PRUEBAAA', 58, 10, 7),
(24, 'Test_prueba', 'Test', 78, 2, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prove_proveedores`
--

CREATE TABLE `prove_proveedores` (
  `prove_id` int(11) NOT NULL,
  `prove_nombre` varchar(50) NOT NULL,
  `prove_tipo_documento` int(11) NOT NULL,
  `prove_no_documento` int(20) NOT NULL,
  `prove_contacto` varchar(20) NOT NULL,
  `prove_correo` varchar(50) NOT NULL,
  `prove_est_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prove_proveedores`
--

INSERT INTO `prove_proveedores` (`prove_id`, `prove_nombre`, `prove_tipo_documento`, `prove_no_documento`, `prove_contacto`, `prove_correo`, `prove_est_id`) VALUES
(1, 'PRUEBAAAAA', 5, 1234, '3164010649', 'PRUEBA@PRUEBA.COM', 10),
(2, 'Carton Colombia SAS', 5, 1455587, 'Carvajal ', '312588', 10),
(3, 'Papeles&Papeles', 5, 8954, 'diana', 'skdjask', 10),
(7, 'Prueba', 5, 8000000, '3153707461', 'dianaq@kaska.com', 10),
(8, 'Prueba2', 5, 805, '3145879', 'a@f.com', 9),
(9, 'Empresa Equis', 5, 2147483647, '3155175058', 'diana1998@gmail.com', 10),
(10, 'DIANA', 2, 1432, '789456', 'SWDASA@F.COM', 10),
(11, 'LJ', 5, 1111111111, 'LAs', 'SWDASA@F.COM', 9),
(12, 'LJ', 1, 2147483647, 'LAs', 'SWDASA@F.COM', 9),
(13, 'ASAS', 2, 213123, '212', 'SWDASA@F.COM', 9),
(14, 'jsdhajsdasjkj', 1, 8888888, '5555555', 'SWDASA@F.COM', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_roles`
--

CREATE TABLE `rol_roles` (
  `rol_id` int(11) NOT NULL,
  `rol_descripcion` varchar(50) NOT NULL,
  `rol_est_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_roles`
--

INSERT INTO `rol_roles` (`rol_id`, `rol_descripcion`, `rol_est_id`) VALUES
(1, 'Administrador', 5),
(4, 'Vendedor', 5),
(5, 'Usuario', 5),
(11, 'Ingeniero', 5),
(12, 'Auxiliar Administrativo', 5),
(13, 'Contador', 5),
(15, 'Asesor', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_menu`
--

CREATE TABLE `sub_menu` (
  `subme_menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `submenu_nombre` varchar(100) NOT NULL,
  `submenu_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sub_menu`
--

INSERT INTO `sub_menu` (`subme_menu_id`, `sub_menu_id`, `submenu_nombre`, `submenu_url`) VALUES
(1, 1, 'Crear cliente', 'form_crear_cliente.php'),
(2, 1, 'Obtener cliente', 'form_obtenerCliente.php'),
(3, 2, 'Crear usuario', 'form_crear_usuario.php'),
(4, 2, 'Obtener usuario', 'form_obtenerUsuarios.php'),
(5, 3, 'Crear rol', 'form_crear_rol.php'),
(6, 3, 'Obtener rol', 'form_obtenerRoles.php'),
(7, 4, 'Crear Proveedor', 'form_crear_proveedor.php'),
(8, 4, 'Obtener proveedor', 'form_obtener_proveedor.php'),
(9, 5, 'Crear producto', 'form_crear_producto.php'),
(10, 5, 'Obtener producto', 'form_obtenerProductos.php'),
(11, 6, 'Crear menu', 'form_crear_menu.php'),
(12, 6, 'Crear submenu', 'form_crear_submenu.php'),
(13, 6, 'Asignar Menus', 'form_gestion_menu.php'),
(15, 6, 'Obtener Menus', 'form_obtener_menus.php'),
(16, 3, 'a', 'a'),
(17, 6, 'Diana', 'a'),
(18, 8, 'Crear venta', 'kkkk');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submenu_usu`
--

CREATE TABLE `submenu_usu` (
  `submeusu_id` int(11) NOT NULL,
  `usu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `submenu_usu`
--

INSERT INTO `submenu_usu` (`submeusu_id`, `usu_id`, `sub_menu_id`) VALUES
(86, 1, 1),
(91, 16, 1),
(95, 16, 5),
(97, 16, 12),
(119, 1, 11),
(124, 1, 3),
(126, 1, 6),
(128, 1, 10),
(132, 1, 5),
(133, 1, 2),
(134, 1, 4),
(135, 1, 8),
(136, 1, 12),
(137, 185, 4),
(138, 185, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_doc_tipodocumento`
--

CREATE TABLE `tipo_doc_tipodocumento` (
  `tipo_doc_id` int(11) NOT NULL,
  `tip_doc_nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_doc_tipodocumento`
--

INSERT INTO `tipo_doc_tipodocumento` (`tipo_doc_id`, `tip_doc_nombre`) VALUES
(1, 'CC'),
(2, 'TI'),
(3, 'Pasaporte'),
(4, 'CE'),
(5, 'NIT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_usuario`
--

CREATE TABLE `usu_usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_login` varchar(50) NOT NULL,
  `usu_clave` varchar(50) NOT NULL,
  `usu_rol_id` int(11) NOT NULL,
  `usu_foto` varchar(100) DEFAULT NULL,
  `usu_est_id` int(11) NOT NULL,
  `super_user` int(11) NOT NULL DEFAULT '0' COMMENT 'super usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usu_usuario`
--

INSERT INTO `usu_usuario` (`usu_id`, `usu_login`, `usu_clave`, `usu_rol_id`, `usu_foto`, `usu_est_id`, `super_user`) VALUES
(1, 'admin', 'admin', 1, '../img_user/692ccc1de0_IMG_20170602_184506857.jpg', 3, 1),
(16, 'ospi', '1234', 4, '../img_user/1b1987e29d_IMG_20170416_111932614.jpg', 3, 0),
(185, 'adriana', '', 4, NULL, 1, 0),
(186, 'joaquin', '', 4, NULL, 1, 0),
(188, 'albania', '', 4, NULL, 1, 0),
(189, 'sandra', '', 4, NULL, 1, 0),
(190, 'emmanuel', 'holi', 4, NULL, 1, 0),
(191, 'dareo', '', 4, NULL, 1, 0),
(193, 'oscar', '', 4, NULL, 1, 0),
(194, 'juancito', '', 4, NULL, 1, 0),
(195, 'diana', '', 4, NULL, 1, 0),
(196, 'pruba', '', 4, NULL, 1, 0),
(197, 'Holi', 'holi', 4, '../img_user/5bc866f29a_IMG_20170602_184506857.jpg', 3, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cli_clientes`
--
ALTER TABLE `cli_clientes`
  ADD PRIMARY KEY (`cli_id`),
  ADD KEY `cli_tipo_documento` (`cli_tipo_documento`),
  ADD KEY `cli_estado` (`cli_estado`);

--
-- Indices de la tabla `est_estado`
--
ALTER TABLE `est_estado`
  ADD PRIMARY KEY (`est_id`),
  ADD KEY `est_gru_id` (`est_gru_id`);

--
-- Indices de la tabla `gru_grupos`
--
ALTER TABLE `gru_grupos`
  ADD PRIMARY KEY (`gru_id`);

--
-- Indices de la tabla `menu_menu`
--
ALTER TABLE `menu_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `produ_producto`
--
ALTER TABLE `produ_producto`
  ADD PRIMARY KEY (`produ_id`),
  ADD KEY `produ_est_id` (`produ_est_id`),
  ADD KEY `produ_prove_id` (`produ_prove_id`);

--
-- Indices de la tabla `prove_proveedores`
--
ALTER TABLE `prove_proveedores`
  ADD PRIMARY KEY (`prove_id`),
  ADD KEY `prove_est_id` (`prove_est_id`),
  ADD KEY `prove_tipo_documento` (`prove_tipo_documento`);

--
-- Indices de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  ADD PRIMARY KEY (`rol_id`),
  ADD KEY `rol_est_id` (`rol_est_id`);

--
-- Indices de la tabla `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`subme_menu_id`),
  ADD KEY `sub_menu_id` (`sub_menu_id`);

--
-- Indices de la tabla `submenu_usu`
--
ALTER TABLE `submenu_usu`
  ADD PRIMARY KEY (`submeusu_id`),
  ADD KEY `usu_id` (`usu_id`),
  ADD KEY `sub_menu_id` (`sub_menu_id`);

--
-- Indices de la tabla `tipo_doc_tipodocumento`
--
ALTER TABLE `tipo_doc_tipodocumento`
  ADD PRIMARY KEY (`tipo_doc_id`);

--
-- Indices de la tabla `usu_usuario`
--
ALTER TABLE `usu_usuario`
  ADD PRIMARY KEY (`usu_id`),
  ADD UNIQUE KEY `usu_login` (`usu_login`),
  ADD KEY `usu_est_id` (`usu_est_id`),
  ADD KEY `usu_usuario_ibfk_3` (`usu_rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cli_clientes`
--
ALTER TABLE `cli_clientes`
  MODIFY `cli_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `est_estado`
--
ALTER TABLE `est_estado`
  MODIFY `est_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `menu_menu`
--
ALTER TABLE `menu_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `produ_producto`
--
ALTER TABLE `produ_producto`
  MODIFY `produ_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `prove_proveedores`
--
ALTER TABLE `prove_proveedores`
  MODIFY `prove_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `subme_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `submenu_usu`
--
ALTER TABLE `submenu_usu`
  MODIFY `submeusu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT de la tabla `tipo_doc_tipodocumento`
--
ALTER TABLE `tipo_doc_tipodocumento`
  MODIFY `tipo_doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `usu_usuario`
--
ALTER TABLE `usu_usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cli_clientes`
--
ALTER TABLE `cli_clientes`
  ADD CONSTRAINT `cli_clientes_ibfk_1` FOREIGN KEY (`cli_tipo_documento`) REFERENCES `tipo_doc_tipodocumento` (`tipo_doc_id`),
  ADD CONSTRAINT `cli_clientes_ibfk_2` FOREIGN KEY (`cli_estado`) REFERENCES `est_estado` (`est_id`);

--
-- Filtros para la tabla `est_estado`
--
ALTER TABLE `est_estado`
  ADD CONSTRAINT `est_estado_ibfk_1` FOREIGN KEY (`est_gru_id`) REFERENCES `gru_grupos` (`gru_id`);

--
-- Filtros para la tabla `produ_producto`
--
ALTER TABLE `produ_producto`
  ADD CONSTRAINT `produ_producto_ibfk_1` FOREIGN KEY (`produ_est_id`) REFERENCES `est_estado` (`est_id`),
  ADD CONSTRAINT `produ_producto_ibfk_2` FOREIGN KEY (`produ_prove_id`) REFERENCES `prove_proveedores` (`prove_id`);

--
-- Filtros para la tabla `prove_proveedores`
--
ALTER TABLE `prove_proveedores`
  ADD CONSTRAINT `prove_proveedores_ibfk_1` FOREIGN KEY (`prove_est_id`) REFERENCES `est_estado` (`est_id`),
  ADD CONSTRAINT `prove_proveedores_ibfk_2` FOREIGN KEY (`prove_tipo_documento`) REFERENCES `tipo_doc_tipodocumento` (`tipo_doc_id`);

--
-- Filtros para la tabla `rol_roles`
--
ALTER TABLE `rol_roles`
  ADD CONSTRAINT `rol_roles_ibfk_1` FOREIGN KEY (`rol_est_id`) REFERENCES `est_estado` (`est_id`);

--
-- Filtros para la tabla `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD CONSTRAINT `sub_menu_ibfk_1` FOREIGN KEY (`sub_menu_id`) REFERENCES `menu_menu` (`id_menu`);

--
-- Filtros para la tabla `submenu_usu`
--
ALTER TABLE `submenu_usu`
  ADD CONSTRAINT `submenu_usu_ibfk_1` FOREIGN KEY (`usu_id`) REFERENCES `usu_usuario` (`usu_id`),
  ADD CONSTRAINT `submenu_usu_ibfk_2` FOREIGN KEY (`sub_menu_id`) REFERENCES `sub_menu` (`subme_menu_id`);

--
-- Filtros para la tabla `usu_usuario`
--
ALTER TABLE `usu_usuario`
  ADD CONSTRAINT `usu_usuario_ibfk_2` FOREIGN KEY (`usu_est_id`) REFERENCES `est_estado` (`est_id`),
  ADD CONSTRAINT `usu_usuario_ibfk_3` FOREIGN KEY (`usu_rol_id`) REFERENCES `rol_roles` (`rol_id`);
--
-- Base de datos: `prueba`
--
CREATE DATABASE IF NOT EXISTS `prueba` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prueba`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `menu_nombre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id_menu` int(11) NOT NULL,
  `subme_id` int(11) NOT NULL,
  `subme_nombre` varchar(100) NOT NULL,
  `subme_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subme_usu`
--

CREATE TABLE `subme_usu` (
  `subme_usu_id` int(11) NOT NULL,
  `usu_id` int(11) NOT NULL,
  `submenu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_usuario`
--

CREATE TABLE `usu_usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_login` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `subme_usu`
--
ALTER TABLE `subme_usu`
  ADD PRIMARY KEY (`subme_usu_id`);

--
-- Indices de la tabla `usu_usuario`
--
ALTER TABLE `usu_usuario`
  ADD PRIMARY KEY (`usu_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usu_usuario`
--
ALTER TABLE `usu_usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT;--
-- Base de datos: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
